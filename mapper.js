// Define spreadsheet URL.
//var mySpreadsheet = 'https://docs.google.com/spreadsheet/ccc?key=1WfU5DS2WAUoJ_nXR8KX4zRkT4YZS4SGcQkPxw0EkyGo#gid=0';
//https://docs.google.com/spreadsheets/d/1WfU5DS2WAUoJ_nXR8KX4zRkT4YZS4SGcQkPxw0EkyGo/edit?usp=sharing
var mySpreadsheet = 'https://docs.google.com/spreadsheets/d/1WfU5DS2WAUoJ_nXR8KX4zRkT4YZS4SGcQkPxw0EkyGo/edit#gid=0';
var map;
var geocoder;

$(document).ready(function(){

	console.log(mySpreadsheet);

	$('#stats').sheetrock({
		url: mySpreadsheet
	});


	initMap();
	
});

$(window).load(function(){
	loadPins();
});



function initMap(){
	var myLatlng = new google.maps.LatLng(43.072957, -89.404704);
	
	var mapOptions = {
		zoom: 6,
		center: myLatlng
	}
	geocoder = new google.maps.Geocoder();
	map = new google.maps.Map(document.getElementById('map-canvas'), 
		mapOptions);

}



function loadPins(map){
	var nameIndex = 1;
	var zipIndex = 4;


	$('#stats tbody tr').each(function(){
		var name = $('td:nth-child(1)', this).text();
		var zipCode = $('td:nth-child(4)', this).text();
		addPinForZip(name, zipCode);
	});

}

function addPinForZip(name, zip){

	geocoder.geocode( { 'address': zip}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        //Got result, center the map and put it out there
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location,
            title: name
        });

        var infoContent = '<div id="content">' +
        	'<p>' + name + '</p></div>';
        var infowindow = new google.maps.InfoWindow({
        	content: infoContent
        });

        google.maps.event.addListener(marker, 'click', function(){
        	infowindow.open(map, marker);
        });

      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
}
